﻿/// ETML
/// Auteur: Sébastien TILLE
/// Date: 10 janvier 2024
/// Description: fenêtre de paramètres de Mastermind.
/// 
/// Structure:
/// 1. Constantes
/// 2. Variables et objets
/// 3. Méthodes utilitaires
/// 4. Evénements

using System;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class frmSettings : Form
    {
        #region Constantes
        /// <summary>
        /// Etat par défaut du mode debug.
        /// </summary>
        private const bool DEFAULT_DEBUG_VALUE = false;

        /// <summary>
        /// Répétition des couleurs.
        /// </summary>
        private const bool DEFAULT_REPEAT_VALUE = true;

        /// <summary>
        /// Nombre de couleurs par défaut.
        /// </summary>
        private const int DEFAULT_NB_COLORS = 7;

        /// <summary>
        /// Longueur par défaut du code à deviner.
        /// </summary>
        private const int DEFAULT_CODE_LENGTH = 4;

        /// <summary>
        /// Palette de couleurs par défaut.
        /// </summary>
        private const string DEFAULT_COLOR_PALETTE = "Pastel";
        #endregion

        #region Variables et objets
        private bool _debug;        // Paramètre actuel de debug
        private bool _repeat;       // Répétition des couleurs
        private int _length;        // Longueur du code
        private int _nbColors;      // Nombre de couleurs
        private string _palette;    // Palette activée
        private frmGame _game;      // Jeu en cours
        #endregion

        /// <summary>
        /// Constructeur du formulaire de paramètres.
        /// </summary>
        /// <param name="game">une instance du jeu Mastermind.</param>
        public frmSettings(frmGame game)
        {
            InitializeComponent();
            this.Icon = Resources.Assets.icon;
            this._game = game;
            _length = game.codeLength;
            _nbColors = game.nbColors;
            _palette = game.palette;
            _debug = game.debug;
            _repeat = game.colorsCanRepeat;
        } //frmMastermind()

        #region Utilitaires
        /// <summary>
        /// Applique les paramètres.
        /// </summary>
        private void ApplySettings()
        {
            _game.codeLength = _length;
            _game.nbColors = _nbColors;
            _game.debug = _debug;
            _game.colorsCanRepeat = _repeat;
        } // ApplySettings();

        /// <summary>
        /// Vérifie si les paramètres sont valides.
        /// </summary>
        /// <param name="length">la longueur du code.</param>
        /// <param name="colors">le nombre de couleurs du code.</param>
        /// <param name="repeat">si les couleurs peuvent se répéter.</param>
        /// <returns>true si les paramètres sont valides, false sinon.</returns>
        private bool SettingsAreValid(int length, int colors, bool repeat)
        {
            if (length > colors && !repeat)
            {
                MessageBox.Show(Resources.Common.SettingRepeatInvalid, Resources.Common.SettingsInvalid,
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        } // SettingsAreValid()
        #endregion

        #region Events
        /// <summary>
        /// Permet de fermer le formulaire sans appliquer les paramètres.
        /// </summary>
        /// <param name="sender">le bouton "annuler".</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        } // btnCancel_Click()

        /// <summary>
        /// Permet de fermer le formulaire en sauvegardant les paramètres.
        /// </summary>
        /// <param name="sender">le bouton "enregistrer".</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            // Applique les paramètre et relance une partie
            ApplySettings();
            _game.ResetGame();

            // Fermer le formulaire
            this.Close();
            Cursor.Current = Cursors.Default;
        } // btnSave_Click()

        /// <summary>
        /// Récupère la valeur des boites numériques, les testes et les appliques si elles
        /// sont valides.
        /// </summary>
        /// <param name="sender">une boite numérique.</param>
        /// <param name="e">l'évènement associé.</param>
        private void nud_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown nud = (NumericUpDown) sender;
            int newValue = (int) nud.Value;

            if (nud.Equals(nudColors))
                if (SettingsAreValid(_length, newValue, _repeat))
                    _nbColors = newValue;
                else
                    nudColors.Value = _nbColors;
            else if (nud.Equals(nudLength))
                if (SettingsAreValid(newValue, _nbColors, _repeat))
                    _length = newValue;
                else
                    nudLength.Value = _length;
        } // nud_ValueChanged()

        /// <summary>
        /// Charge les propritétés lors du chargement du formulaire.
        /// </summary>
        /// <param name="sender">un formulaire de paramètres.</param>
        /// <param name="e">l'évènement associé.</param>
        private void frmSettings_Load(object sender, EventArgs e)
        {
            nudLength.Value = _length;
            nudColors.Value = _nbColors;
            cbPalette.Text = _palette;
            chkRepeat.Checked = _repeat;
        } // frmSettings_Load()

        /// <summary>
        /// Permet de réinitialiser l'ensemble des paramètres du jeu.
        /// </summary>
        /// <param name="sender">le bouton "réinitialiser".</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnResetSettings_Click(object sender, EventArgs e)
        {
            // Demander la confirmation à l'utilisateur
            DialogResult reset;
            reset = MessageBox.Show(Resources.Common.ResetInfo, Resources.Common.ResetTitle,
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (reset != DialogResult.Yes)
                return;

            // Réinitialisation et sauvegarde des paramètres
            Properties.Settings.Default.NbColors = _nbColors = DEFAULT_NB_COLORS;
            Properties.Settings.Default.CodeLength =_length = DEFAULT_CODE_LENGTH;
            Properties.Settings.Default.Debug = _debug = DEFAULT_DEBUG_VALUE;
            Properties.Settings.Default.ColorPalette = _palette = DEFAULT_COLOR_PALETTE;
            Properties.Settings.Default.Repeat = _repeat = DEFAULT_REPEAT_VALUE;
            Properties.Settings.Default.Save();

            // Appliquer les paramètres
            ApplySettings();
            _game.ResetGame();
            this.Close();
        } // btnReset_Click()

        /// <summary>
        /// Permet de changer la palette de couleur du jeu.
        /// </summary>
        /// <param name="sender">un menu déroulant.</param>
        /// <param name="e">l'évènement associé.</param>
        private void cbPalette_SelectedIndexChanged(object sender, EventArgs e)
        {
            _palette = cbPalette.Text;
            _game.ChangeColorPalette(_palette);
            Properties.Settings.Default.ColorPalette = _palette;
        } // cbPalette_SelectedIndexChanged()

        /// <summary>
        /// Permet d'autoriser ou interdire l'utilisation de la même couleur plusieurs
        /// fois dans le même code.
        /// </summary>
        /// <param name="sender">une checkbox.</param>
        /// <param name="e">l'évènement associé.</param>
        private void chkRepeat_Click(object sender, EventArgs e)
        {
            bool validSettings = SettingsAreValid(_length, _nbColors, !_repeat);

            // Changer le paramètre si valide, sinon laisser
            if (validSettings)
                _repeat = !_repeat;
            else
                chkRepeat.Checked = !chkRepeat.Checked;
        } // chkRepeat_Click()
        #endregion
    }
}
