﻿/// ETML
/// Auteur:         Sébastien TILLE
/// Date:           10 janvier 2024
/// Description:    Classe de couleurs liées à Mastermind. Elle permet de retourner une palette de couleur.
/// 
/// Inspiré de la classe System.Drawing.Color et du lien suivant
/// https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/choosing-between-class-and-struct

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mastermind.Utils
{
    public struct Colors
    {     
        // Palette "Pastel"
        public static Color PastelGreen => Color.FromArgb(255, 192, 255, 192);
        public static Color PastelYellow => Color.FromArgb(255, 255, 255, 192);
        public static Color PastelWhite => Color.FromArgb(255, 255, 255, 255);
        public static Color PastelRed => Color.FromArgb(255, 255, 192, 192);
        public static Color PastelBlue => Color.FromArgb(255, 150, 192, 255);
        public static Color PastelMagenta => Color.FromArgb(255, 255, 192, 255);
        public static Color PastelCyan => Color.FromArgb(255, 128, 255, 255);

        public static Color[] PastelPalette => new[] { 
            PastelGreen, 
            PastelYellow, 
            PastelWhite, 
            PastelRed, 
            PastelBlue, 
            PastelMagenta, 
            PastelCyan
        };

        // Palette "Classic"
        public static Color[] ClassicPalette => new[] {
            Color.Green,
            Color.Yellow,
            Color.White,
            Color.Red,
            Color.Blue,
            Color.Magenta,
            Color.Cyan
        };

        // Palette "Tol"
        // https://personal.sron.nl/~pault/
        public static Color TolGreen => Color.FromArgb(255, 34, 136, 51);
        public static Color TolYellow => Color.FromArgb(255, 204, 187, 68);
        public static Color TolWhite => Color.FromArgb(255, 187, 187, 187);
        public static Color TolRed => Color.FromArgb(255, 238, 102, 119);
        public static Color TolBlue => Color.FromArgb(255, 68, 119, 170);
        public static Color TolMagenta => Color.FromArgb(255, 170, 51, 119);
        public static Color TolCyan => Color.FromArgb(255, 102, 204, 238);

        public static Color[] TolPalette => new[] {
            TolGreen,
            TolYellow,
            TolWhite,
            TolRed,
            TolBlue,
            TolMagenta,
            TolCyan
        };

        // Palette violette
        public static Color MesaGreen => Color.FromArgb(255, 155, 155, 122);
        public static Color MesaYellow => Color.FromArgb(255, 217, 174, 148);
        public static Color MesaWhite => Color.FromArgb(255, 229, 197, 158);
        public static Color MesaRed => Color.FromArgb(255, 241, 220, 167);
        public static Color MesaBlue => Color.FromArgb(255, 228, 176, 116);
        public static Color MesaMagenta => Color.FromArgb(255, 208, 140, 96);
        public static Color MesaCyan => Color.FromArgb(255, 153, 123, 102);

        // Palette "Mesa"
        public static Color[] MesaPalette => new[] {
            MesaGreen,
            MesaYellow,
            MesaWhite,
            MesaRed,
            MesaBlue,
            MesaMagenta,
            MesaCyan
        };

        // Palette "Lavender"
        public static Color LavenderGreen => Color.FromArgb(255, 236, 224, 245);
        public static Color LavenderYellow => Color.FromArgb(255, 216, 193, 235);
        public static Color LavenderWhite => Color.FromArgb(255, 197, 163, 225);
        public static Color LavenderRed => Color.FromArgb(255, 168, 116, 210);
        public static Color LavenderBlue => Color.FromArgb(255, 149, 86, 200);
        public static Color LavenderMagenta => Color.FromArgb(255, 129, 60, 185);
        public static Color LavenderCyan => Color.FromArgb(255, 101, 47, 144);

        public static Color[] LavenderPalette => new[] {
            LavenderGreen,
            LavenderYellow,
            LavenderWhite,
            LavenderRed,
            LavenderBlue,
            LavenderMagenta,
            LavenderCyan
        };

        // Palette "Tritanopia"
        public static Color TritanopiaGreen => Color.FromArgb(255, 121, 199, 83);
        public static Color TritanopiaYellow => Color.FromArgb(255, 255, 196, 86);
        public static Color TritanopiaWhite => Color.FromArgb(255, 255, 255, 255);
        public static Color TritanopiaRed => Color.FromArgb(255, 232, 77, 69);
        public static Color TritanopiaBlue => Color.FromArgb(255, 112, 146, 190);
        public static Color TritanopiaMagenta => Color.FromArgb(255, 178, 114, 182);
        public static Color TritanopiaCyan => Color.FromArgb(255, 75, 172, 198);

        public static Color[] TritanopiaPalette => new[] {
            TritanopiaGreen,
            TritanopiaYellow,
            TritanopiaWhite,
            TritanopiaRed,
            TritanopiaBlue,
            TritanopiaMagenta,
            TritanopiaCyan
        };
    }
}
