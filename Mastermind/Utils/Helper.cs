﻿/// ETML
/// Auteur:         Sébastien TILLE
/// Date:           10 janvier 2024
/// Description:    Classe d'utilitaires divers.

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind.Utils
{
    /// <summary>
    /// Classe utilisée pour divers utilitaires.
    /// </summary>
    internal class Helper
    {
        /// <summary>
        /// Définit la couleur de fond d'un label.
        /// </summary>
        /// <param name="l">le label dont la couleur de fond doit être changée.</param>
        /// <param name="c">la couleur a utiliser.</param>
        public static void SetLabelColors(Label label, Color color)
        {
            label.BackColor = color;
        } // SetLabelColor()

        /// <summary>
        /// Permet de déplacer un point donné d'une certaine distance dans l'axe horizontal et vertical.
        /// </summary>
        /// <param name="pt">Le point initial.</param>
        /// <param name="dx">Le déplacement horizontal.</param>
        /// <param name="dy">Le déplacement vertical.</param>
        /// <returns></returns>
        public static Point Offset(Point pt, int dx, int dy)
        {
            return new Point(pt.X + dx, pt.Y + dy);
        } // Offset()
    }
}
