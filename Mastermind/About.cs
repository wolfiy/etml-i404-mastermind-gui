﻿/// ETML
/// Auteur: Sébastien TILLE
/// Date: 10 janvier 2024
/// Description: fenêtre d'informations de Mastermind
/// 
/// Structure:
/// 1. Constantes
/// 2. Evénements

using System;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class frmAbout : Form
    {
        /// <summary>
        /// Lien vers le répertoire GitLab de Mastermind.
        /// </summary>
        private const string URL_GITLAB = "https://gitlab.com/wolfiy/etml-i404-mastermind-gui";

        /// <summary>
        /// Constructeur de la fenêtre "à propos".
        /// </summary>
        public frmAbout()
        {
            InitializeComponent();
            this.Icon = Resources.Assets.icon;
            this.Text = Resources.Common.AboutTitle;
            lblAboutTitle.Text = Resources.Common.AboutHeader;
            lblAboutDesc.Text = Resources.Common.AboutInfo;
            llblGitlab.Text = URL_GITLAB;
            btnClose.Text = Resources.Common.ButtonClose;
        } // frmAbout()

        /// <summary>
        /// Permet de fermer le formulaire.
        /// </summary>
        /// <param name="sender">le bouton fermer.</param>
        /// <param name="e">l'événement associé.</param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } // btnClose_Click()

        /// <summary>
        /// Ouvre dans le navigateur par défaut le répertoire GitLab associé au projet.
        /// </summary>
        /// <param name="sender">le lien du répertoire.</param>
        /// <param name="e">l'événement associé.</param>
        private void llblGitlab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(URL_GITLAB);
        } // llblGitlab_LinkClicked()
    }
}
