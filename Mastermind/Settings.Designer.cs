﻿namespace Mastermind
{
    partial class frmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSettings));
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblCodeLength = new System.Windows.Forms.Label();
            this.lblColors = new System.Windows.Forms.Label();
            this.nudLength = new System.Windows.Forms.NumericUpDown();
            this.nudColors = new System.Windows.Forms.NumericUpDown();
            this.btnResetSettings = new System.Windows.Forms.Button();
            this.cbPalette = new System.Windows.Forms.ComboBox();
            this.lblPalette = new System.Windows.Forms.Label();
            this.chkRepeat = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColors)).BeginInit();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            resources.ApplyResources(this.btnCancel, "btnCancel");
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            resources.ApplyResources(this.btnSave, "btnSave");
            this.btnSave.Name = "btnSave";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblCodeLength
            // 
            resources.ApplyResources(this.lblCodeLength, "lblCodeLength");
            this.lblCodeLength.Name = "lblCodeLength";
            // 
            // lblColors
            // 
            resources.ApplyResources(this.lblColors, "lblColors");
            this.lblColors.Name = "lblColors";
            // 
            // nudLength
            // 
            resources.ApplyResources(this.nudLength, "nudLength");
            this.nudLength.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.nudLength.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudLength.Name = "nudLength";
            this.nudLength.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudLength.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // nudColors
            // 
            resources.ApplyResources(this.nudColors, "nudColors");
            this.nudColors.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.nudColors.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudColors.Name = "nudColors";
            this.nudColors.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudColors.ValueChanged += new System.EventHandler(this.nud_ValueChanged);
            // 
            // btnResetSettings
            // 
            resources.ApplyResources(this.btnResetSettings, "btnResetSettings");
            this.btnResetSettings.Name = "btnResetSettings";
            this.btnResetSettings.UseVisualStyleBackColor = true;
            this.btnResetSettings.Click += new System.EventHandler(this.btnResetSettings_Click);
            // 
            // cbPalette
            // 
            this.cbPalette.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPalette.FormattingEnabled = true;
            this.cbPalette.Items.AddRange(new object[] {
            resources.GetString("cbPalette.Items"),
            resources.GetString("cbPalette.Items1"),
            resources.GetString("cbPalette.Items2"),
            resources.GetString("cbPalette.Items3"),
            resources.GetString("cbPalette.Items4"),
            resources.GetString("cbPalette.Items5")});
            resources.ApplyResources(this.cbPalette, "cbPalette");
            this.cbPalette.Name = "cbPalette";
            this.cbPalette.SelectedIndexChanged += new System.EventHandler(this.cbPalette_SelectedIndexChanged);
            // 
            // lblPalette
            // 
            resources.ApplyResources(this.lblPalette, "lblPalette");
            this.lblPalette.Name = "lblPalette";
            // 
            // chkRepeat
            // 
            resources.ApplyResources(this.chkRepeat, "chkRepeat");
            this.chkRepeat.Name = "chkRepeat";
            this.chkRepeat.UseVisualStyleBackColor = true;
            this.chkRepeat.Click += new System.EventHandler(this.chkRepeat_Click);
            // 
            // frmSettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chkRepeat);
            this.Controls.Add(this.lblPalette);
            this.Controls.Add(this.cbPalette);
            this.Controls.Add(this.btnResetSettings);
            this.Controls.Add(this.nudColors);
            this.Controls.Add(this.nudLength);
            this.Controls.Add(this.lblColors);
            this.Controls.Add(this.lblCodeLength);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSettings";
            this.Load += new System.EventHandler(this.frmSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudColors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblCodeLength;
        private System.Windows.Forms.Label lblColors;
        private System.Windows.Forms.NumericUpDown nudLength;
        private System.Windows.Forms.NumericUpDown nudColors;
        private System.Windows.Forms.Button btnResetSettings;
        private System.Windows.Forms.ComboBox cbPalette;
        private System.Windows.Forms.Label lblPalette;
        private System.Windows.Forms.CheckBox chkRepeat;
    }
}