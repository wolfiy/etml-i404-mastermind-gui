﻿namespace Mastermind
{
    partial class frmAbout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.lblAboutTitle = new System.Windows.Forms.Label();
            this.lblAboutDesc = new System.Windows.Forms.Label();
            this.llblGitlab = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(12, 147);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Fermer";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblAboutTitle
            // 
            this.lblAboutTitle.AutoSize = true;
            this.lblAboutTitle.Location = new System.Drawing.Point(12, 9);
            this.lblAboutTitle.Name = "lblAboutTitle";
            this.lblAboutTitle.Size = new System.Drawing.Size(61, 13);
            this.lblAboutTitle.TabIndex = 4;
            this.lblAboutTitle.Text = "Mastermind";
            // 
            // lblAboutDesc
            // 
            this.lblAboutDesc.Location = new System.Drawing.Point(12, 31);
            this.lblAboutDesc.Name = "lblAboutDesc";
            this.lblAboutDesc.Size = new System.Drawing.Size(381, 95);
            this.lblAboutDesc.TabIndex = 5;
            this.lblAboutDesc.Text = "[description]";
            // 
            // llblGitlab
            // 
            this.llblGitlab.AutoSize = true;
            this.llblGitlab.Location = new System.Drawing.Point(12, 121);
            this.llblGitlab.Name = "llblGitlab";
            this.llblGitlab.Size = new System.Drawing.Size(39, 13);
            this.llblGitlab.TabIndex = 6;
            this.llblGitlab.TabStop = true;
            this.llblGitlab.Text = "[Repo]";
            this.llblGitlab.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llblGitlab_LinkClicked);
            // 
            // frmAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 182);
            this.Controls.Add(this.llblGitlab);
            this.Controls.Add(this.lblAboutDesc);
            this.Controls.Add(this.lblAboutTitle);
            this.Controls.Add(this.btnClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAbout";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "A propos";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblAboutTitle;
        private System.Windows.Forms.Label lblAboutDesc;
        private System.Windows.Forms.LinkLabel llblGitlab;
    }
}