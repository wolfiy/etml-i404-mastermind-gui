﻿/// ETML
/// Auteur: Sébastien TILLE
/// Date: 10 janvier 2024
/// Description: fenêtre principale de Mastermind
/// 
/// Structure:
/// 1. Constantes
/// 2. Variables
/// 3. Objets
/// 4. Initialisation
/// 5. Logique
/// 6. Utilitaires
/// 7. Evénements

using Mastermind.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class frmGame : Form
    {
        #region Constantes
        /// <summary>
        /// Largeur du conteneur de couleurs (boutons).
        /// </summary>
        private const int COLORS_CONTAINER_WIDTH = 1;

        /// <summary>
        /// Nombre maximal d'essais.
        /// </summary>
        private const int MAX_GUESS = 10;

        /// <summary>
        /// Espacement des labels.
        /// </summary>
        private const int SPACING = 5;

        /// <summary>
        /// Position du premier élément (panGuess).
        /// </summary>
        private readonly Point TOP_LEFT = new Point(12, 30);

        /// <summary>
        /// Taille des boutons et labels.
        /// </summary>
        private readonly Size SQUARE_LARGE = new Size(32, 32);

        /// <summary>
        /// Taille des indices.
        /// </summary>
        private readonly Size SQUARE_SMALL = new Size(10, 10);
        #endregion

        #region Variables
        // Paramètres de jeu
        private bool _win;          // Etat du jeu
        private bool _debug;        // Affichage du code
        public bool debug
        {
            get { return _debug; }
            set { _debug = value; }
        }

        private bool _colorsRepeat; // Répétition des couleurs
        public bool colorsCanRepeat
        {
            get { return _colorsRepeat; }
            set { _colorsRepeat = value;  }
        }

        private int _codeLength;    // Longueure du code
        public int codeLength 
        {
            get { return _codeLength; }
            set { _codeLength = value; } 
        }

        private int _nbColors;      // Nombre de couleurs
        public int nbColors         
        {
            get { return _nbColors; }
            set { _nbColors = value; }
        }

        private string _palette;    // Palette de couleur
        public string palette
        {
            get { return _palette; }
            set { _palette = value; }
        }

        // Eléments de jeu
        private int _round;         // Nombre d'essais
        private int _inputIndex;    // Nb couleurs insérées
        private Label[,] _labels;   // Essais
        private Label[,] _hints;    // Indices
        private Color[] _code;      // Code à trouver
        private Color[] _input;     // Code entré
        private Color[] _colors;    // Couleurs utilisées
        #endregion

        #region Objets
        /// <summary>
        /// Fenêtre contenant des informations sur le jeu.
        /// </summary>
        private Form _about;

        /// <summary>
        /// Fenêtre contenant les règles du jeu.
        /// </summary>
        private Form _help;

        /// <summary>
        /// Fenêtre de paramétrisation du jeu.
        /// </summary>
        private Form _settings;

        /// <summary>
        /// Générateur de nombres aléatoires utilisé pour le code.
        /// </summary>
        private Random _rdm;

        /// <summary>
        /// Type de cellule.
        /// </summary>
        private enum CellType
        {
            Guess,
            Hint,
            Debug
        }
        #endregion

        /// <summary>
        /// Constructeur du de la fenêtre du jeu.
        /// </summary>
        public frmGame()
        {
            // Préparation de base
            InitializeComponent();
            this.Icon = Resources.Assets.icon;
            _rdm = new Random();

            // Propriété sauvegardées
            _codeLength = Properties.Settings.Default.CodeLength;
            _debug = Properties.Settings.Default.Debug;
            _nbColors = Properties.Settings.Default.NbColors;
            _palette = Properties.Settings.Default.ColorPalette;
            _colorsRepeat = Properties.Settings.Default.Repeat;

            // Mise en place du jeu
            ResetGame();
            DebugDisplay(_debug);
        } // frmGame()

        #region Initialisation
        /// <summary>
        /// Réinitialise le jeu et initialise les éléments.
        /// </summary>
        public void ResetGame()
        {
            // Curseur de chargement
            Cursor.Current = Cursors.WaitCursor;

            // Reinitialisation des attributs
            _palette = Properties.Settings.Default.ColorPalette;
            _colors = ChangeColorPalette(_palette);
            _code = GenerateCode(_colorsRepeat);
            _round = 0;
            _inputIndex = 0;
            _codeLength = codeLength;
            _nbColors = nbColors;

            _labels = new Label[_codeLength, MAX_GUESS + 1];
            _hints = new Label[_codeLength, MAX_GUESS];
            _input = new Color[_codeLength];
            _win = false;

            // Vider les panels
            foreach (Panel p in Controls.OfType<Panel>())
                p.Controls.Clear();

            // Remplir les panels
            FillTableWithLabels(panGuess, _codeLength, MAX_GUESS, CellType.Guess);
            FillTableWithLabels(panHints, _codeLength, MAX_GUESS, CellType.Hint);
            FillTableWithLabels(panAnswers, _codeLength, 1, CellType.Debug);
            FillTableWithButtons(panColors, COLORS_CONTAINER_WIDTH, _nbColors);

            // Couleur de fond des labels
            foreach (Label label in _labels)
                Helper.SetLabelColors(label, SystemColors.Control);

            // Affichage debug et réactivation des boutons
            menuDebug.Checked = _debug;
            DebugDisplay(menuDebug.Checked);
            btnValidate.Enabled = true;
            btnClear.Enabled = true;

            // Placement des composants
            PlaceItems();
        } // ResetGame()

        /// <summary>
        /// Place les éléments de la fenêtre.
        /// </summary>
        private void PlaceItems()
        {
            // Espacement
            int margin = panGuess.Margin.Left;
            int marginHints = 2;
            int marginColors = 6;

            // Positions
            Point ptHint = Helper.Offset(TOP_LEFT, panGuess.Width + marginHints * margin, 0);
            Point ptColors = Helper.Offset(ptHint, panHints.Width + marginColors * margin, 0);
            Point ptFound = Helper.Offset(panAnswers.Location, panAnswers.Width + margin, 0);
            Point ptMisplaced = Helper.Offset(ptFound, 0, SQUARE_LARGE.Height - lblFound.Height);

            // Mise en place
            panGuess.Location = TOP_LEFT;
            panHints.Location = ptHint;
            panColors.Location = ptColors;
            lblFound.Location = ptFound;
            lblMisplaced.Location = ptMisplaced;
        } // PlacePanels()

        /// <summary>
        /// Permet de remplir un panneau de boutons.
        /// </summary>
        /// <param name="panel">le panneau sur lequel placer les boutons.</param>
        /// <param name="columns">le nombre de colonnes du panneau.</param>
        /// <param name="rows">le nombre de lignes du panneau.</param>
        private void FillTableWithButtons(TableLayoutPanel panel, int columns, int rows)
        {
            // Gestion du panneau
            panel.ColumnCount = columns;
            panel.RowCount = rows;
            panel.Width = SQUARE_LARGE.Width;
            panel.Height = (SQUARE_LARGE.Height + SPACING) * _nbColors;
            panel.AutoSize = true;

            // Placer les boutons
            for (int r = 0; r < rows; ++r)
            {
                for (int c = 0; c < columns; ++c)
                {
                    Button btn = new Button();
                    btn.AutoSize = false;
                    btn.BackColor = _colors[r];
                    btn.Click += new EventHandler(btnColor_Click);
                    btn.Size = SQUARE_LARGE;
                    panel.Controls.Add(btn, c, r);
                }
            }
        } // FillTableWithButtons()

        /// <summary>
        /// Permet de remplir un panneau de labels.
        /// </summary>
        /// <param name="panel">le panneau sur lequel placer les labels.</param>
        /// <param name="columns">le nombre de colonnes.</param>
        /// <param name="rows">le nombre de lignes.</param>
        /// <param name="type">le type de cellule.</param>
        /// <exception cref="InvalidEnumArgumentException">si le type n'existe pas.</exception>
        private void FillTableWithLabels(TableLayoutPanel panel, int columns, int rows, CellType type)
        {
            // Lignes et colonnes
            panel.ColumnCount = columns;
            panel.RowCount = rows;
            panel.AutoSize = true;
            panel.RowStyles.Clear();
            panel.ColumnStyles.Clear();

            // Placer les labels
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    Label lbl = new Label();
                    lbl.Anchor = AnchorStyles.None;
                    lbl.AutoSize = false;
                    lbl.BorderStyle = BorderStyle.FixedSingle;
                    lbl.Size = SQUARE_LARGE;

                    switch (type)
                    {
                        case CellType.Debug:
                            _labels[j, MAX_GUESS] = lbl;
                            break;
                        case CellType.Guess:
                            _labels[j, i] = lbl;
                            break;
                        case CellType.Hint:
                            lbl.Size = SQUARE_SMALL;
                            _hints[j, i] = lbl;
                            break;
                        default:
                            throw new InvalidEnumArgumentException();
                    }

                    // Ajout du label au panneau associé
                    panel.Controls.Add(lbl, j, i);
                }
            }

            // Ajout des styles
            for (int i = 0; i < rows; ++i)
                panel.RowStyles.Add(new RowStyle(SizeType.Absolute, SQUARE_LARGE.Height + SPACING));

            for (int i = 0; i < columns; ++i)
                panel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100 / _codeLength));
        } // FillTableWithLabels()
        #endregion

        #region Logique
        /// <summary>
        /// Génère aléatoirement un code constitué de couleurs.
        /// </summary>
        /// <param name="repeat">répétition possible des couleurs.</param>
        /// <returns>un tableau contenant les couleurs du code.</returns>
        private Color[] GenerateCode(bool repeat)
        {
            int index;
            Color[] code = new Color[_codeLength];

            // Génération individuelle des couleurs
            for (int i = 0; i < _codeLength; ++i)
            {
                index = _rdm.Next(_nbColors);
                Color c = _colors[index];

                // Empêcher la même couleur d'apparaitre plusieurs fois si demandé
                if (!repeat)
                {
                    do
                    {
                        index = _rdm.Next(_nbColors);
                        c = _colors[index];
                    }
                    while (code.Contains(c));
                }
               code[i] = c;
            }

            return code;
        } // GenerateCode()

        /// <summary>
        /// Vérifie la validité d'une combinaison.
        /// </summary>
        /// <param name="input">la combinaison à vérifier.</param>
        /// <param name="code">la combinaison valide.</param>
        /// <returns>true si la combinaison est trouvée, false sinon.</returns>
        private bool CheckCombination(Color[] input, Color[] code)
        {
            // Compteur de bonnes positions.
            int foundColors = 0;
            int misplacedColors = 0;

            // Tableaux gardant en mémoire si une couleur a été trouvée / vérifiée.
            bool[] doNotCheckCode = new bool[_codeLength];
            bool[] doNotCheckInput = new bool[_codeLength];

            // Si toutes les couleurs sont au bon endroit
            if (input.SequenceEqual(code))
            {
                return true;
            }

            // Recherche des bonnes couleurs au bon endroit
            for (int i = 0; i < _codeLength; ++i)
            {
                if (_code[i] == _input[i])
                {
                    ++foundColors;
                    doNotCheckCode[i] = doNotCheckInput[i] = true;
                }
            }

            // Recherche des couleurs mal placées
            for (int i = 0; i < _codeLength; ++i)
            {
                // Couleurs qui n'ont pas été trouvée précédamment.
                if (!doNotCheckCode[i])
                {
                    // Parcours des couleurs restantes.
                    for (int j = 0; j < _codeLength; ++j)
                    {
                        // Une couleur est mal placée si elle n'a pas été
                        // trouvée. Le cas i = j a déjà été traité dans les
                        // cas "ok".
                        if (!doNotCheckInput[j] && _code[i] == _input[j]
                            && i != j)
                        {
                            ++misplacedColors;
                            doNotCheckCode[i] = true;
                            doNotCheckInput[j] = true;
                            break;
                        }
                    }
                }
            }

            // Affichage de debug
            lblFound.Text = Resources.Common.DebugTextFound + foundColors;
            lblMisplaced.Text = Resources.Common.DebugTextMisplaced + misplacedColors;
            ColorHints(foundColors, misplacedColors);

            return false;
        } // CheckCombination()

        /// <summary>
        /// Permet de colorer les indices.
        /// </summary>
        /// <param name="black">le nombre de couleurs trouvées.</param>
        /// <param name="red">le nombre de couleurs mal placées.</param>
        private void ColorHints(int black, int red)
        {
            for (int i = 0; i < black + red; ++i)
            {
                Helper.SetLabelColors(_hints[i, _round], i < black ? Color.Black
                                                                   : Color.Red);
            }
        } // ColorHints()

        /// <summary>
        /// Affiche la combinaison, le message de fin du jeu et désactive les boutons.
        /// </summary>
        /// <param name="win">l'état de la partie.</param>
        private void EndGame(bool win)
        {
            // Afficher la réponse
            DebugDisplay(true);

            // Message de fin
            MessageBox.Show(win ? Resources.Common.OutcomeMessageWin 
                                : Resources.Common.OutcomeMessageLose);

            btnValidate.Enabled = false;
            btnClear.Enabled = false;
        } // EndGame()
        #endregion

        #region Utilitaires
        /// <summary>
        /// Permet de changer la palette de couleur du jeu.
        /// </summary>
        /// <param name="theme">le thème à appliquer.</param>
        /// <returns>le tableau de couleurs correspondant.</returns>
        public Color[] ChangeColorPalette(string theme)
        {
            switch (theme)
            {
                case "Classic":
                    return Colors.ClassicPalette;
                case "Tol (colorblind)":
                    return Colors.TolPalette;
                case "Mesa":
                    return Colors.MesaPalette;
                case "Lavender":
                    return Colors.LavenderPalette;
                case "Tritanopia":
                    return Colors.TritanopiaPalette;
                case "Pastel":
                default:
                    return Colors.PastelPalette;
            }
        } // ChangeColorPalette()

        /// <summary>
        /// Permet de changer la langue du jeu.
        /// </summary>
        /// <param name="lang">langue au format ISO.</param>
        private void ChangeLanguage(string lang)
        {
            // Changement de langue
            CultureInfo ci = new CultureInfo(lang);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;

            // Réaffichage des éléments
            this.Controls.Clear();
            InitializeComponent();
            ResetGame();
        } // ChangeLanguage()

        /// <summary>
        /// Affiche le code sur la dernière ligne du panneau de 'guess'.
        /// </summary>
        private void DebugDisplay(bool b)
        {
            // Afficher ou masquer les éléments de la dernière ligne
            for (int i = 0; i < _codeLength; ++i)
            {
                _labels[i, MAX_GUESS].BackColor = b ? _code[i]
                                                    : SystemColors.Control;
            }

            // Afficher les éléments de debug si demandé
            _debug = lblFound.Visible = lblMisplaced.Visible = menuDebug.Checked;
        } // DebugDisplay()

        /// <summary>
        /// Permet de sauvegarder le paramètres courants.
        /// </summary>
        /// <param name="sender">l'objet source.</param>
        /// <param name="e">l'évènement associé.</param>
        private void SaveSettings()
        {
            // Entrée des paramètres
            Properties.Settings.Default.CodeLength = _codeLength;
            Properties.Settings.Default.NbColors = _nbColors;
            Properties.Settings.Default.Debug = _debug;
            Properties.Settings.Default.Repeat = _colorsRepeat;
            Properties.Settings.Default.ColorPalette = _palette;

            // Sauvegarde
            Properties.Settings.Default.Save();
        } // SaveSettings()
        #endregion

        #region Events
        /// <summary>
        /// Permet de colorer un 'guess' en cliquant sur une couleur.
        /// </summary>
        /// <param name="sender">le bouton correspondant à la couleur.</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnColor_Click(object sender, EventArgs e)
        {
            Button button = (Button) sender;

            if (_inputIndex < _codeLength)
            {
                Helper.SetLabelColors(_labels[_inputIndex, _round], button.BackColor);
                ++_inputIndex;
            }
            else 
            {
                MessageBox.Show(String.Format(Resources.Common.MessageMaxLength, _codeLength));
            }
        } // btnColor_Click()

        /// <summary>
        /// Permet de vider la ligne courante.
        /// </summary>
        /// <param name="sender">le bouton vider.</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnClear_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < _codeLength; ++i)
            {
                Helper.SetLabelColors(_labels[i, _round], SystemColors.Control);
                _inputIndex = 0;
            }
        } // btnClear_Click()

        /// <summary>
        /// Permet de quitter le jeu.
        /// </summary>
        /// <param name="sender">le bouton quitter.</param>
        /// <param name="e">l'évènement associé.</param>
        private void btnQuit_Click(object sender, EventArgs e)
        {
            // Sauvegarder
            SaveSettings();

            // Récupérer le choix de l'utilisateur
            DialogResult restart;
            restart = MessageBox.Show(Resources.Common.QuitInfo, Resources.Common.QuitTitle,
                                      MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            // Quitter s'il séléctionne "oui".
            if (restart == DialogResult.Yes)
                Environment.Exit(0);
        } // btnQuit_Click()

        /// <summary>
        /// Permet de valider une entrée.
        /// </summary>
        /// <param name="sender">le bouton valider.</param>
        /// <param name="e">l'événement associé.</param>
        private void btnValidate_Click(object sender, EventArgs e)
        {
            // Continuer si toutes les cases ont été remplies
            if (_inputIndex < _codeLength && _round < MAX_GUESS)
            {
                MessageBox.Show(String.Format(Resources.Common.MessageLength, _codeLength));
                return;
            }

            // Stockage de l'input
            for (int i = 0; i < _codeLength; ++i)
                _input[i] = _labels[i, _round].BackColor;

            // Vérification
            _win = CheckCombination(_code, _input);
            if (_win)
            {
                EndGame(_win);
                return;
            }

            // Fin du jeu
            if (_round < MAX_GUESS - 1)
                ++_round;
            else
                EndGame(_win);
            _inputIndex = 0;
        } // btnValidate_Click()

        /// <summary>
        /// Change la langue du programme en fonction de l'item sélectionné dans le menu.
        /// </summary>
        /// <param name="sender">le ToolStripMenuItem correspondant à la langue.</param>
        /// <param name="e">l'évènement associé.</param>
        private void tsmiLanguage_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem)sender;

            if (item.Equals(menuEnglish))
                ChangeLanguage("en");
            else if (item.Equals(menuFrench))
                ChangeLanguage("fr");
        } // tsmiLanguage_Click()

        /// <summary>
        /// Permet d'ouvrir un nouveau formulaire.
        /// </summary>
        /// <param name="sender">le ToolStripMenuItem correspondant au formulaire à ouvrir.</param>
        /// <param name="e">l'évènement associé.</param>
        private void tsmiPaneOpener_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem) sender;

            if (item.Equals(menuAbout))
            {
                _about = new frmAbout();
                _about.ShowDialog();
            }
            else if (item.Equals(menuRules))
            {
                _help = new frmHelp();
                _help.Show();
            }
            else if (item.Equals(menuMore))
            {
                _settings = new frmSettings(this);
                _settings.ShowDialog();
            }
        } // tsmiPaneOpener_Click()

        /// <summary>
        /// Gère les interactions avec les ToolStripMenuItems.
        /// </summary>
        /// <param name="sender">l'item sélectionné.</param>
        /// <param name="e">l'évènement associé.</param>
        private void tsmiHandler_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem item = (ToolStripMenuItem) sender;

            if (item.Equals(menuRestart))
            {
                DialogResult restart;
                restart = MessageBox.Show(Resources.Common.RestartInfo, Resources.Common.RestartTitle, 
                                          MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                
                if (restart == DialogResult.Yes) 
                    ResetGame();
            }
            else if (item.Equals(menuDebug))
            {
                menuDebug.Checked = !menuDebug.Checked;
            }
        } // tsmiHandler_Click()

        /// <summary>
        /// Gère le bouton d'affichage du debug.
        /// </summary>
        /// <param name="sender">le 'menu item' debug.</param>
        /// <param name="e">l'événement associé.</param>
        private void tsmiDebug_CheckedChanged(object sender, EventArgs e)
        {
            _debug = !_debug;
            DebugDisplay(_debug);
        } // tsmiDebug_CheckedChanged()

        /// <summary>
        /// Règle les paramètre du formulaire principal lors du chargement de ce dernier.
        /// </summary>
        /// <param name="sender">le formulaire.</param>
        /// <param name="e">l'évènement associé.</param>
        private void frmMastermind_Load(object sender, EventArgs e)
        {
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
        } // frmMastermind_Load()

        /// <summary>
        /// Sauvegarde les paramètres lorsque la fenêtre est fermée.
        /// </summary>
        /// <param name="sender">le bouton fermer.</param>
        /// <param name="e">l'évènement associé.</param>
        private void frmMastermind_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveSettings();
        } // frmMastermind_FormClosing()
        #endregion
    }
}
