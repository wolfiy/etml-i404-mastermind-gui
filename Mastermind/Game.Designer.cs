﻿namespace Mastermind
{
    partial class frmGame
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGame));
            this.menuGame = new System.Windows.Forms.MenuStrip();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLanguage = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEnglish = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFrench = new System.Windows.Forms.ToolStripMenuItem();
            this.menuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.menuMore = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRestart = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRules = new System.Windows.Forms.ToolStripMenuItem();
            this.menuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.btnValidate = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnQuit = new System.Windows.Forms.Button();
            this.panAnswers = new System.Windows.Forms.TableLayoutPanel();
            this.panGuess = new System.Windows.Forms.TableLayoutPanel();
            this.lblFound = new System.Windows.Forms.Label();
            this.lblMisplaced = new System.Windows.Forms.Label();
            this.panHints = new System.Windows.Forms.TableLayoutPanel();
            this.panColors = new System.Windows.Forms.TableLayoutPanel();
            this.menuGame.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuGame
            // 
            this.menuGame.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSettings,
            this.menuRestart,
            this.menuHelp});
            resources.ApplyResources(this.menuGame, "menuGame");
            this.menuGame.Name = "menuGame";
            // 
            // menuSettings
            // 
            this.menuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLanguage,
            this.menuDebug,
            this.menuMore});
            this.menuSettings.Name = "menuSettings";
            resources.ApplyResources(this.menuSettings, "menuSettings");
            // 
            // menuLanguage
            // 
            this.menuLanguage.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuEnglish,
            this.menuFrench});
            this.menuLanguage.Name = "menuLanguage";
            resources.ApplyResources(this.menuLanguage, "menuLanguage");
            // 
            // menuEnglish
            // 
            this.menuEnglish.Name = "menuEnglish";
            resources.ApplyResources(this.menuEnglish, "menuEnglish");
            this.menuEnglish.Click += new System.EventHandler(this.tsmiLanguage_Click);
            // 
            // menuFrench
            // 
            this.menuFrench.Name = "menuFrench";
            resources.ApplyResources(this.menuFrench, "menuFrench");
            this.menuFrench.Click += new System.EventHandler(this.tsmiLanguage_Click);
            // 
            // menuDebug
            // 
            this.menuDebug.Name = "menuDebug";
            resources.ApplyResources(this.menuDebug, "menuDebug");
            this.menuDebug.CheckedChanged += new System.EventHandler(this.tsmiDebug_CheckedChanged);
            this.menuDebug.Click += new System.EventHandler(this.tsmiHandler_Click);
            // 
            // menuMore
            // 
            this.menuMore.Name = "menuMore";
            resources.ApplyResources(this.menuMore, "menuMore");
            this.menuMore.Click += new System.EventHandler(this.tsmiPaneOpener_Click);
            // 
            // menuRestart
            // 
            this.menuRestart.Name = "menuRestart";
            resources.ApplyResources(this.menuRestart, "menuRestart");
            this.menuRestart.Click += new System.EventHandler(this.tsmiHandler_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuRules,
            this.menuAbout});
            this.menuHelp.Name = "menuHelp";
            resources.ApplyResources(this.menuHelp, "menuHelp");
            // 
            // menuRules
            // 
            this.menuRules.Name = "menuRules";
            resources.ApplyResources(this.menuRules, "menuRules");
            this.menuRules.Click += new System.EventHandler(this.tsmiPaneOpener_Click);
            // 
            // menuAbout
            // 
            this.menuAbout.Name = "menuAbout";
            resources.ApplyResources(this.menuAbout, "menuAbout");
            this.menuAbout.Click += new System.EventHandler(this.tsmiPaneOpener_Click);
            // 
            // btnValidate
            // 
            resources.ApplyResources(this.btnValidate, "btnValidate");
            this.btnValidate.Name = "btnValidate";
            this.btnValidate.UseVisualStyleBackColor = true;
            this.btnValidate.Click += new System.EventHandler(this.btnValidate_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnQuit
            // 
            resources.ApplyResources(this.btnQuit, "btnQuit");
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // panAnswers
            // 
            resources.ApplyResources(this.panAnswers, "panAnswers");
            this.panAnswers.Name = "panAnswers";
            // 
            // panGuess
            // 
            resources.ApplyResources(this.panGuess, "panGuess");
            this.panGuess.Name = "panGuess";
            // 
            // lblFound
            // 
            resources.ApplyResources(this.lblFound, "lblFound");
            this.lblFound.Name = "lblFound";
            // 
            // lblMisplaced
            // 
            resources.ApplyResources(this.lblMisplaced, "lblMisplaced");
            this.lblMisplaced.Name = "lblMisplaced";
            // 
            // panHints
            // 
            resources.ApplyResources(this.panHints, "panHints");
            this.panHints.Name = "panHints";
            // 
            // panColors
            // 
            resources.ApplyResources(this.panColors, "panColors");
            this.panColors.Name = "panColors";
            // 
            // frmGame
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panColors);
            this.Controls.Add(this.panHints);
            this.Controls.Add(this.lblMisplaced);
            this.Controls.Add(this.lblFound);
            this.Controls.Add(this.panGuess);
            this.Controls.Add(this.panAnswers);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnValidate);
            this.Controls.Add(this.menuGame);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuGame;
            this.MaximizeBox = false;
            this.Name = "frmGame";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMastermind_FormClosing);
            this.Load += new System.EventHandler(this.frmMastermind_Load);
            this.menuGame.ResumeLayout(false);
            this.menuGame.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuGame;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.ToolStripMenuItem menuLanguage;
        private System.Windows.Forms.ToolStripMenuItem menuEnglish;
        private System.Windows.Forms.ToolStripMenuItem menuFrench;
        private System.Windows.Forms.ToolStripMenuItem menuRestart;
        private System.Windows.Forms.ToolStripMenuItem menuHelp;
        private System.Windows.Forms.ToolStripMenuItem menuRules;
        private System.Windows.Forms.ToolStripMenuItem menuAbout;
        private System.Windows.Forms.Button btnValidate;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.TableLayoutPanel panAnswers;
        private System.Windows.Forms.TableLayoutPanel panGuess;
        private System.Windows.Forms.Label lblFound;
        private System.Windows.Forms.Label lblMisplaced;
        private System.Windows.Forms.TableLayoutPanel panHints;
        private System.Windows.Forms.TableLayoutPanel panColors;
        private System.Windows.Forms.ToolStripMenuItem menuMore;
        private System.Windows.Forms.ToolStripMenuItem menuDebug;
    }
}

