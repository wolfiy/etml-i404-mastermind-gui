﻿/// ETML
/// Auteur: Sébastien TILLE
/// Date: 10 janvier 2024
/// Description: fenêtre d'aide de Mastermind

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mastermind
{
    public partial class frmHelp : Form
    {
        /// <summary>
        /// Constructeur d'une fenêtre d'aide.
        /// </summary>
        public frmHelp()
        {
            InitializeComponent();
            InitializeRules();
            this.Icon = Resources.Assets.icon;
            btnClose.Text = Resources.Common.ButtonClose;
        } // frmHelp()

        /// <summary>
        /// Configuration du panneau d'aide.
        /// </summary>
        private void InitializeRules()
        {
            this.Text = Resources.Common.HelpTitle;
            lblHelpTitle.Text = Resources.Common.HelpHeader;
            lblHelpRules.Text = Resources.Common.HelpInfo;
        } // InitializeRules()

        #region Events
        /// <summary>
        /// Ferme la fenêtre d'aide.
        /// </summary>
        /// <param name="sender">le bouton fermer.</param>
        /// <param name="e">l'évènement.</param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        } // btnClose_Click()
        #endregion
    }
}
