
# Mastermind GUI
Projet de programmation, ETML, 2023-2024

![Menu principal](screenshots/won.PNG)

## Description
"Mastermind" est une implémentation en C# du jeu de société du même nom qui a été réalisée dans le cadre du second projet de programmation de l'ETML.

Le cahier des charges est disponible [ici](https://gitlab.com/wolfiy/etml-i404-mastermind-gui/-/raw/master/docs/P_PROG-T2-CDC.pdf).

### Fonctionnalités
Les fonctionnalités suivantes ont été implémentées et testés:
- Jeu de base
- Longeur de code variable
- Nombre de couleurs variable
- Menu d'aide
- Différentes palettes de couleurs
- Langue française et anglaise
- Les paramètres persistent après la fermeture du jeu
- Possibilité de n'avoir chaque couleur qu'une seule fois

L'ensemble des fonctionnalités requises et optionnelles fonctionnent.

Remarque: pour que les traductions fonctionnent, ne pas oublier de placer le dossier "en" à côté de l'exécutable (cf. dossier binaries)!

### Captures d'écran
![Paramètres](screenshots/settings.PNG)

![Demonstration](screenshots/demo.mp4)

## Documentation utilisée
### Localisation
MSDN, [Walkthrough: Localizing Windows Forms](https://learn.microsoft.com/en-us/previous-versions//y99d1cd3(v=vs.85)?redirectedfrom=MSDN)  
MSDN, [CultureInfo.CurrentCulture Property](https://learn.microsoft.com/en-us/dotnet/api/system.globalization.cultureinfo.currentculture?view=net-8.0)  

### Sauvegarde des paramètres
Pulp Free Press, [Persisting C#.NET Application Settings To The App.config File](https://www.youtube.com/watch?v=fM1RcyNFRCE)  